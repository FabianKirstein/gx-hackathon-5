package testproto

import (
	"testing"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/cam/internal/protobuf"
	"google.golang.org/protobuf/reflect/protoreflect"
	"google.golang.org/protobuf/types/known/anypb"
	"google.golang.org/protobuf/types/known/structpb"
)

// NewAny creates a new anypb.Any out of a protobuf message and panics on error.
func NewAny(t *testing.T, m protoreflect.ProtoMessage) (a *anypb.Any) {
	var err error

	a, err = anypb.New(m)
	if err != nil {
		t.Error(err)
	}

	return
}

// ToValue is a wrapper around [protobuf.ToValue] which fails the test on error.
func ToValue(t *testing.T, strct any) (value *structpb.Value) {
	var err error

	value, err = protobuf.ToValue(strct)
	if err != nil {
		t.Error(err)
	}

	return
}
