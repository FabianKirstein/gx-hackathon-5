package testutil

import "context"

type key struct{}

func ToContext[T any](ctx context.Context, value *T) context.Context {
	return context.WithValue(ctx, key{}, value)
}

func FromContext[T any](ctx context.Context) *T {
	if out, ok := ctx.Value(key{}).(*T); ok {
		return out
	}

	return nil
}
