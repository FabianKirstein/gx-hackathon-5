package storage

import (
	"clouditor.io/clouditor/persistence"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// VerifyExistence verifies that at least one entry exists for the field given by column and value. Returns gRPC error
// if not.
func VerifyExistence(storage persistence.Storage, entryType string, table any, column, value string) (err error) {
	count, err := storage.Count(table, "%s = ?", column, value)
	if err != nil {
		err = status.Errorf(codes.Internal, "DB error: %v", err)
		return
	}
	if count == 0 {
		err = status.Errorf(codes.NotFound, "No %s found for '%s' (%s)", entryType, value, column)
		return
	}
	return
}
