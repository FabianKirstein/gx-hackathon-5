// Copyright (c) 2022 Fraunhofer AISEC
// Fraunhofer-Gesellschaft zur Foerderung der angewandten Forschung e.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import "./style.scss"
import "bootstrap/dist/js/bootstrap.js"
import { createApp } from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faGear, faGears, faTrash, faPenToSquare, faBook } from '@fortawesome/free-solid-svg-icons'
import App from './App.vue'
import router from './router/router'

library.add(faTrash, faGear, faGears, faPenToSquare, faBook)

const app = createApp(App)
    .component('font-awesome-icon', FontAwesomeIcon)

app.use(router)
app.mount('#app')



