import { OidcClient, settings } from "./settings";
import jwt_decode from "jwt-decode";

var client = new OidcClient(settings);

export function login() {
    signin()
}

export function logout() {
    setToken(null);
    signout();
}

export function isAuthenticated() {
    let token = localStorage.getItem("token")

    if (token == null) {
        // If the token is empty, we are not authenticated
        return false;
    }

    // Try to parse the token
    try {
        // Check the expiry of the token, to see whether we are still authenticated
        const claims = jwt_decode(token);
        return claims.exp >= new Date().getTime() / 1000;
    }
    catch (err) {
        // If the token is invalid, we are definitely not authenticated
        return false;
    }
}

export function setToken(ptoken) {
    localStorage.setItem('token', ptoken)
}

export function getToken() {
    return localStorage.getItem('token')
}

export function setIdToken(ptoken) {
    localStorage.setItem('id_token', ptoken)
}

export function getIdToken() {
    return localStorage.getItem('id_token')
}


// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

///////////////////////////////
// OidcClient config
///////////////////////////////

function log() {
    Array.prototype.forEach.call(arguments, function (msg) {
        if (msg instanceof Error) {
            msg = "Error: " + msg.message;
        }
        else if (typeof msg !== "string") {
            msg = JSON.stringify(msg, null, 2);
        }
    });
}



///////////////////////////////
// functions for UI elements
///////////////////////////////
function signin() {
    client.createSigninRequest({ state: { bar: 15 } }).then(function (req) {
        log("signin request", req, "<a href='" + req.url + "'>go signin</a>");
        // if (followLinks()) {
        window.location = req.url;
        //}
    }).catch(function (err) {
        console.error(err);
        log(err);
    });
}

var signinResponse;
export function processSigninResponse(url) {
    console.log("url:" + url);
    client.processSigninResponse().then(function (response) {
        signinResponse = response;
        console.log("response:" + response);
        log("signin response", signinResponse);

    }).catch(function (err) {
        console.error(err);
        log(err);
    });
}

function signout() {
    client.createSignoutRequest({ id_token_hint: getIdToken() }).then(function (req) {
        log("signout request", req, "<a href='" + req.url + "'>go signout</a>");
        // if (followLinks()) {
        window.location = req.url;
        //}
    }).catch(function (err) {
        console.error(err);
        log(err);
    });
}

export function processSignoutResponse() {
    client.processSignoutResponse().then(function (response) {
        signinResponse = null;
        log("signout response", response);
    }).catch(function (err) {
        console.error(err);
        log(err);
    });
}

export {
    log
};