# CAM - Continuous Automated Monitoring

Welcome to the Hacking Session 4!

## Presentation Slides
[Gaia-X-hacking-session.pdf](../Gaia-X-hacking-session.pdf)

## Hacking Session Walkthrough
In this hacking session, we assume that we have found a new technical description for a requirement in the control System Hardening - which is neither adressed already nor any existing collection module can check it.
The CM we want to create should check if a service or node is alive or not. i.e. doing a healthcheck.

In the following, we provide a guideline for creating a new collection module (CM) and all things that need to be done in addition.
In summary, this task comprises the following steps:
- Creating a metric (technical description of an requirement, e.g. part of a control in the EUCS)
- Defining a configuration for the CM (in accordance to the metric definition)
- Implement the CM
- Update Dashboard (we did that already for you)

As a general hint that applies to almost all tasks: If you get stuck, look the (similar) code to get inspired.
But, of course, please first try it out on your own.

### What You Need
All you need is Docker and VS Code (including the "Remote - Container" extension) installed.
Experience with programming in Go and thats it.
Otherwise, you will need at least a go compiler (go 1.19), protobuf compiler and node.js (npm).

### Preparation
- Install Docker and VS Code (with the *Remote-Container* extension)
- Clone this repository (or download it, if you don't have/want to install Git)
- Open VS Code, go to *File - Open Workspace from File* and choose *cam.workspace* in the cloned/downloaded repository
- Optional (needs Git): Make your own branch from main. Follow this naming scheme: `hacking/<YourUserName>`
- Optional (needs Git): At the end, make a **Draft** merge request. So we can check the solutions
- Open the *Command Palette* (e.g. via *F1* key), type "Remote-Containers: Rebuild and Reopen in Container" and press enter (now it will open the project in the pre-defiden Docker environmen. This may take a couple of minutes)

### Creating a Metric
Before there is a new collection module, there has to be the reason for it - a new metric.
This metric first has to be defined and included into the CAM service.
In the future, we plan to make the creation of metrics part of the UI.
But for now, we have to get our hands dirty and enter it into the code by ourselves.
Look at the *./policies/bundles/* folder to see, which metrics are currently in place.
#### Task 1.1 Write New Metric
Your first task here is to add a metric *HealthCheck* by creating a folder with that name (notice the CamelCase!) and describe it via the *metric.rego* / *data.json* files (see the presentation slides for more information about those files. Or as always, feel free to ask!).
Hint: What we want is a simple *status* true or false for our healthcheck.
#### Task 1.2 Update Oscal File
Add metric to our OSCAL file ./gxfs.json* (be sure it is exactly the same as the filename ;)).
Remember, that we assume the metric to address the *System Hardening* Control (OPS-21).
#### Task 1.3 Update Metric configuration
Add the metric configuration in ./metrics.json*.
Remember that the range is of binary nature (either the healthcheck is *true* or *false*).

#### SW Stack
Rego (Open Policy Agent), OSCAL
### Defining Configuration for CM
Perfect, we now have incorporated the metric which will be used by the Evaluation Manager to evaluate incoming evidences of the Collection Module which we will implement in a moment.
But before doing so, we have to define the configuration for our healthcheck module.
It is important since the user (the auditor) has to know what he or she has to enter (visualized in the dashboard).

#### Task 2
Go to the API definition of our collection modules, the collection interface written in protobuf: ./api/collection/collection.proto*.
At the bottom, define a new configuration message *HealthCheckConfig* which contains an *url* field of type string.
Since we already implemented the Dashboard, be sure to keep these two namings. Otherwise, the Requirement Manager can't understand incoming configurations from the Dashboard later on when we are running this whole thing.

#### SW Stack
Protobuf
### Implement the Collection Module
Now we come to the part where the magic happens.
Upon a `StartCollectiong` request (in form of a remote procedure call, RPC. See collection interface in the API directory) of the Requirement Manager (which in turns is triggered by the auditor in the Dashboard), a collection module gathers the required information based on the passed configuration (Task 2) to build a so-called evidence.
Then, this evidence is finally sent to the Evaluation Manager via the `SendEvidence` RPC (see evaluation interface in the API directory).

#### Task 3.1 Set Up the Code Base
Create a new service `healthcheck` in the ./service* directory:
- Create a `Service` struct which implements the server side of the collection interface (*gRCP* tip: Embed the `UnimplementedCollectionServer` struct)
- Create a `Value` struct representing the value of an evidence. It has to be aligned to structure you definde in the metric (use *JSON* tags). For technical reasons of Evaluation Manager is using the Clouditor behind the scenes, you have to use also a (anonymous) field of type `voc.Resource`

Note: We ignore the `StopCollecting` RPC.
TL;DR: It will be excluded in the next updated specification.
According to the current specification, a collection module should continuously collecting evidences - until the Requirement Manager stops it via this RPC.
But we made a design decision to let the collection module be "dumb" as possible, meaning it should be stateless and just be executed as a one time job on each start call.
This *fire and forget* approach makes the `StopCollecting` RPC needless, of course.
Among other things, this will be updated in the new specification in a following project.

#### Task 3.2 Implement Evidence Collection
Now implement the `StartCollectiong` method - the healthcheck:
- To implement the healthcheck, use whatever method you think is appropriate. For the sake of time, it doesn't have to be overly complicated, though.
- When creating the evidence (there might be sth. in the API ;)):
    - Use your own defined *Value* of previous task. Take a look at the internal package to find a way for converting it.
    - You have to set at least the fields *id* (non-empty string) and *type* (at least one non-empty element) of *voc.Resource*. Otherwise the Evalution will fail later on (technical reasons of the used *Clouditor*)
    - For the *ToolId* field, create a new constant in *internal/config/config.go*. Generate (e.g. with a generator in the internet) and use a valid UUID. We will need it later on again.
- Send the evidence via the *Send* method of the evaluation interface (in this case, you are the client). Tip: Leverage the *StreamsOf* type of the Clouditor API (*clouditor.io/clouditor/api*)
- We all love writing tests: 
    - Add some tests for `StartCollectiong` to mainly test the validation part
    - Add some tests for the actual collection of evidence. For that, you can extract this functionality in a separate method and test it
    - Optional: If you really want, you could also mock the Evaluation Manager to test that the right evidences are sent upon a `StartCollectiong` request. 

#### Task 3.3 Implement Executable File
A typical structure of *Go* projects contains a *cmd* directory at the root level.
This directory is the place where binaries are built, e.g. for each service a subfolder containing a single "main" Go file.
In this case, our new Collection Module is also a service:
- Create the respective subfolder in *./cmd* (naming is not crucial here, but the best practice is to stick to the existing scheme that is already in place.)
- In your service code of task 3.2, implement a `NewServer` method (a "constructor") which creates a `collection.CollectionServer` to creates a new healthcheck service with default values. Tip: We don't need OAuth in localhost. Thus, you can use an insecure connection to the evaluation stream (use `insecure.NewCredentials()`)
- Implement the "main" file:
    - Create a gRCP server
    - Create a `HealthCheck` service with `NewServer` (Pass the insecure gRCP option here if you don't hardcoded it in `NewServer`)
    - Pass it to the gRCP Server
    - Use next available port for CMs: *50055*
- Optional: Create a Dockerfile which builds it. We will not use it here, but we would need it for production in the Kubernetes cluster.
#### SW Stack
Go (Golang)

### Update the Requirements Manager
We have to add the new collection module to the Requirements Manager.
In the next phase, we will likely add this functionality directly to the Dashboard.
But for now, we use the `autoCreateCollectionModules` method in the Requirements Manager which will create all collection modules at the start when running it (it is pre-configured in the VS Code run configuration to pass the respective argument).

#### Task 4: Add Collection Module to Configuration
Go to *./cmd/cam-req-manager/cam-req-manager.go* and add the new *HealthCheck* module in `autoCreateCollectionModules`.
- Tip 1: The metric name must be identical to the respective folder name of the metric.
- Tip 2: As `ID` use the `ToolID` you created earlier in *configs*
- Tip 3: For the `ConfigMessageTypeUrl`, use `protobuf.TypeURL` to extract the URL from the healthcheck configuration you created in Task 2

### Update Dashboard (not part of this hacking session)
We did that for you.
In addition to the configuration, we may also have to define a new control.
But as mentioned earlier, we just assume it to be part of the *System Hardening* control to keep it simple here.
#### SW Stack
Vue

### Start It!
- Open the file *.vscode/launch.json* and look for the *healthcheck* config. We already wrote it down, but you may have to change the part to yor *cmd* directory.
- Then go to the *Run and Debug* View (e.g., press *CTRL+SHIFT+d*)
- And start the "Launch all services" config
- Now you can use your browser and go to "localhost:8080" and use it :) (You may have to press the login button but since we are in localhost there is no login required. It should instantly be logged in then)
