package evaluation

const (
	DatabaseErrorMsg                 = "DB error"
	EvidenceNotFoundErrorMsg         = "No evidence found"
	NoEvidencesInDBErrorMsg          = "There are no evidences for this service in the DB yet"
	ComplianceResultNotFoundErrorMsg = "Compliance result not found"
)
