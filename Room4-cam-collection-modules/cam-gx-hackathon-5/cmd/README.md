# Microservices 

This `cmd` folder contains the main entry points for the individual microservices and additional binaries that comprises the overall CAM service.

Usually, the main entry point should only consist of a small `main()` function (in Go). Service specific implementations should then reside in the `service/xxx` folders, for example `service/collection/workload` for the Workload Configuration Collection Module.

| Name                       | Description                                      | Default Port |
| -------------------------- | ------------------------------------------------ | ------------ |
| `cam`                      | The command line client to interact with the CAM | -            |
| `cam-api-gateway`          | The CAM API gateway (including the Dashboard)    | 8080         |
| `cam-req-manager`          | The Requirements Manager                         | 50100        |
| `cam-evaluator`            | The Evaluator                                    | 50101        |
| `cam-collection-commsec`   | Communication Security Collection Module         | 50051        |
| `cam-collection-authsec`   | Authentication Security Collection Module        | 50052        |
| `cam-collection-integrity` | Remote Integrity Collection Module               | 50053        |
| `cam-collection-workload`  | Workload Configuration Collection Module         | 50054        |
| `cam-collection-registry`  | Public Registry Collection Module                | 50055        |
